// Package log provides a simple structured logging interface, with
// fancy colors and number line
package log

import (
	"fmt"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

const (
	info    = "INFO"
	debug   = "DEBUG"
	warning = "WARN"
	err     = "ERROR"
)

var (
	// EnableDebug is a flag that's up to the caller to set.
	EnableDebug = false

	colors = map[string]string{
		info:    "\x1b[1m",
		err:     "\x1b[1;31m",
		debug:   "\x1b[1;32m",
		warning: "\x1b[1;33m",
	}
)

func log(level string, format string, args ...interface{}) {
	if level == debug && !EnableDebug {
		return
	}

	t := time.Now().Format("15:04:05")

	// Get current line and file
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	frame, _ = frames.Next()
	filename := filepath.Base(frame.File)
	function := strings.SplitAfter(frame.Function, ".")

	fmt.Printf("%s %s%-5s\x1b[0m \x1b[90m%s:%d\x1b[0m %s: ", t, colors[level], level, filename, frame.Line, function[len(function)-1])
	if format == "" {
		fmt.Println(args...)
		return
	}

	fmt.Printf(format, args...)
	fmt.Println()
}

// Info level logging
func Info(args ...interface{}) {
	log(info, "", args...)
}

// Debug level logging
func Debug(args ...interface{}) {
	log(debug, "", args...)
}

// Debugf level logging
func Debugf(format string, args ...interface{}) {
	log(debug, format, args...)
}

// Warning level logging
func Warning(args ...interface{}) {
	log(warning, "", args...)
}

// Error level logging
func Error(args ...interface{}) {
	log(err, "", args...)
}
